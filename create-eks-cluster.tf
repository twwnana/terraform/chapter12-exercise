variable "env" {}
variable "instance_types" {}
variable "app_name" {}

data "aws_caller_identity" "current" {}

module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "19.17.2"

  cluster_name                   = var.cluster_name
  cluster_version                = "1.28"
  cluster_endpoint_public_access = true

  subnet_ids = module.vpc.private_subnets
  vpc_id     = module.vpc.vpc_id
  tags = {
    environment = "development"
    application = var.app_name
  }

  enable_irsa = true

  cluster_addons = {
    coredns = {
      most_recent                 = true
      resolve_conflicts_on_update = false
      resolve_conflicts_on_create = true
    }
    kube-proxy = {
      most_recent                 = true
      resolve_conflicts_on_update = false
      resolve_conflicts_on_create = true
    }
    vpc-cni = {
      most_recent                 = true
      resolve_conflicts_on_update = false
      resolve_conflicts_on_create = true
    }

    aws-ebs-csi-driver = {
      service_account_role_arn    = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/${var.cluster_name}-ebs-csi-controller"
      resolve_conflicts_on_update = false
      resolve_conflicts_on_create = true
    }
  }

  # cluster_security_group_additional_rules = {
  #   ingress_nodes_ephemeral_ports_tcp = {
  #     description                = "Nodes on ephemeral ports"
  #     protocol                   = "tcp"
  #     from_port                  = 1025
  #     to_port                    = 65535
  #     type                       = "ingress"
  #     source_node_security_group = true
  #   }
  # }

  # worker nodes
  eks_managed_node_groups = {

    aws_launch_template = {
      name = "${var.cluster_name}-ng"
    }
    nodegroup = {
      use_custom_templates = false
      instance_types       = [var.instance_types]
      node_group_name      = "${var.cluster_name}-ng"

      min_size     = 1
      max_size     = 3
      desired_size = 1

      labels = {
        Production = "ng"
      }
      tags = {
        Name = "${var.env}"
        node = "eks-ng"
      }
    }
  }

  fargate_profiles = {
    profile = {
      name = "fargate"
      selectors = [
        {
          namespace = "fargate"
          labels = {
            Production = "fargate"
          }
        }
      ]
      tags = {
        Name = "${var.env}"
      }
    }
  }
}

# starting from EKS 1.23 CSI plugin is needed for volume provisioning.
# https://stackoverflow.com/questions/75758115/persistentvolumeclaim-is-stuck-waiting-for-a-volume-to-be-created-either-by-ex
# module "fully-loaded-eks-cluster_aws-ebs-csi-driver" {
#   source  = "bootlabstech/fully-loaded-eks-cluster/aws//modules/kubernetes-addons/aws-ebs-csi-driver"
#   version = "1.0.7"
#   # insert the 1 required variable here

#   enable_amazon_eks_aws_ebs_csi_driver = true

#   addon_context = {
#     aws_region_name                = "ap-southeast-1"
#     aws_caller_identity_account_id = string
#     aws_caller_identity_arn        = string
#     aws_eks_cluster_endpoint       = ""
#     aws_partition_id               = string
#     eks_cluster_id                 = string
#     eks_oidc_issuer_url            = string
#     eks_oidc_provider_arn          = string
#     tags                           = map(string)
#     irsa_iam_role_path             = string
#     irsa_iam_permissions_boundary  = string
#   }
# }
